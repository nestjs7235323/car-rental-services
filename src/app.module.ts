import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule, JwtService } from '@nestjs/jwt'
import { join } from 'path';
import { GraphQLModule } from '@nestjs/graphql'
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { AuthModule } from './auth/auth.module';
import { User } from './users/user.entity';
import { UsersModule } from './users/users.module';
import { jwtConstants } from './auth/jwt.secret';
import { CarsModule } from './cars/cars.module';
import { Car } from './cars/entities/car.entity';
import { ReservationsModule } from './reservations/reservations.module';
import { Reservation } from './reservations/entities/reservation.entity';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
    }),
    JwtModule.register({
      global: true,
      signOptions:{expiresIn: '1200s'},
      secret: jwtConstants.secret,

    }),
    TypeOrmModule.forRoot({
          type: 'sqlite',
          database: 'main.sqlite',
          entities: [User,Car,Reservation],
          synchronize: true
    }),
    AuthModule,
    UsersModule,
    CarsModule,
    ReservationsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
