import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthResolver } from './auth.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';
import { LocalStrategy } from './strategies/local.strategy';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from 'src/users/users.module';
import { JwtStrategy } from './strategies/jwt.strategy';


@Module({

  imports:[
    TypeOrmModule.forFeature([User]),
    PassportModule,
    UsersModule,
  ],
  providers: [AuthService, AuthResolver , UsersService , LocalStrategy, JwtStrategy]
})
export class AuthModule {}
