import { Args, Context, Mutation, Query, Resolver } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { User } from '../users/user.entity';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { LoginResponseDto } from './types/login.response';
import { loginUserInput } from './types/login-user.input';
import { UseGuards } from '@nestjs/common';
import { GqlAuthGuard } from './guards/auth.guard';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { CurrentUser } from '../decorators/current-user.decorator';




@Resolver(of => User)
export class AuthResolver {
    constructor (
        private readonly authService: AuthService,
        private readonly usersService: UsersService
        ){}


    @Query(returns => [User],{name: 'users'})
    @UseGuards(JwtAuthGuard)
    async users(@CurrentUser() user: User): Promise <User[]>{
        console.log(user)
        return this.usersService.findAllUsers()
    }

    @Mutation(returns => User)
    async signUp(@Args('data')   data: CreateUserDto): Promise<User>{
        return this.authService.signUp(data);
    }


    @Mutation(()=> LoginResponseDto)
    @UseGuards(GqlAuthGuard)
    async signIn (@Args('loginUserInput') data: loginUserInput, @Context() context){
        return this.authService.signIn(context.user)
    }

}
