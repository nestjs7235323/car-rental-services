import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { User } from '../users/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from './jwt.secret';



@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User) private readonly repo: Repository<User>,
        private readonly usersSerive: UsersService,
        private readonly jwtService: JwtService
        ){}

        
        async validateUser(username: string, password: string) {

            const user =  await this.usersSerive.findOne(username);

            if (!user) throw new UnauthorizedException();

            const isMatch = await bcrypt.compare(password , user.password);

            if (user && isMatch){
                const {password, ...result} = user;
                return result;
            }

            return false;
            
        }

        async signUp(data : CreateUserDto): Promise<User> {
            const hashedPassword =  await bcrypt.hash(data.password,10);
            data.password = hashedPassword; 
            return await this.usersSerive.createNewUser(data);
        }




        async signIn(user: User){
            if (!user) throw new NotFoundException;
            
            const payload = {
                username: user.username, 
                sub: user.id
            }

            return {
                access_token: this.jwtService.sign(payload,{secret: jwtConstants.secret}),
                user,
            }

        }



}
