import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { CarsService } from './cars.service';
import { CreateCarInput } from './types/create-car.input';
import { UpdateCarInput } from './types/update-car.input';
import { Car } from './entities/car.entity';
import { RemoveCarResponse } from './types/remove-car.response';

@Resolver('Car')
export class CarsResolver {
  constructor(private readonly carsService: CarsService) {}
  
  @Mutation(()=> Car)
  createCar(@Args('createCarInput') createCarInput: CreateCarInput) {
    return this.carsService.create(createCarInput);
  }

  @Query(returns => [Car],{name: 'Cars'})
  findAll() {
    return this.carsService.findAll();
  }

  @Query(returns => Car,{name: 'Car'})
  findOne(@Args('id') id: number) {
    return this.carsService.findOne(id);
  }

  @Mutation(returns => Car)
  updateCar(@Args('updateCarInput') updateCarInput: UpdateCarInput) {
    return this.carsService.update(updateCarInput.id, updateCarInput);
  }

  @Mutation(returns => RemoveCarResponse)
  async removeCar(@Args('id') id: number) {
    const user = await this.carsService.remove(id);
    return {message: `The car with ${id} has been deleted.`};
  }
}
