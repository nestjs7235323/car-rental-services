import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCarInput } from './types/create-car.input';
import { UpdateCarInput } from './types/update-car.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Car } from './entities/car.entity';


@Injectable()
export class CarsService {
  constructor(@InjectRepository(Car) private readonly repo: Repository<Car>) {}

  create(createCarInput: CreateCarInput): Promise<Car> {
    const car = this.repo.create(createCarInput);

    return this.repo.save(car);
  }

  findAll() {
    return `This action returns all cars`;
  }

  async findOne(id: number) {
    return await this.repo.findOne({
      where: {
        id,
      },
    });
  }

  async update(
    id: number,
    updateCarInput: Partial<UpdateCarInput>,
  ): Promise<Car> {
    let car = await this.findOne(id);
    if (!car) {
      throw new NotFoundException('Car Not Found');
    }
  

    car = {
      id,
      make: updateCarInput.make ?? car.make,
      model: updateCarInput.model ?? car.model,
      year: updateCarInput.year ?? car.year,
      category: updateCarInput.category ?? car.category,
      pricePerDay: updateCarInput.pricePerDay ?? car.pricePerDay,
      reservations: []
    };

    return this.repo.save(car);
  }

  async remove(id: number) {
    const car =  await this.findOne(id);
    if(!car){
      throw new NotFoundException(`The car with id${id} is not found.`)
    }

    return this.repo.delete(id);
  }
}
