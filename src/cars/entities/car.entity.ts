import { Field,Int, ObjectType } from "@nestjs/graphql";
import { Reservation } from "src/reservations/entities/reservation.entity";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";


@Entity()
@ObjectType()
export class Car {
    
    @PrimaryGeneratedColumn()
    @Field(type => Int)
    id: number;

    @Column()
    @Field()
    make: string;

    @Column()
    @Field()
    model: string;

    @Column()
    @Field()
    year: number;

    @Column()
    @Field()
    category: string;

    @Column()
    @Field()
    pricePerDay: number;

    @OneToMany(() => Reservation, reservation => reservation.car)
    reservations: Reservation[];

    
}
