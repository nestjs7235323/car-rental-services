import { Field, InputType } from "@nestjs/graphql";


@InputType()
export class CreateCarInput {

    @Field()
    make: string;

    @Field()
    model: string;

    @Field()
    year: number;

    @Field()
    category: string;

    @Field()
    pricePerDay: number;

}