import { ObjectType, Field } from '@nestjs/graphql';

@ObjectType()
export class RemoveCarResponse {
  @Field()
  message: string;
}