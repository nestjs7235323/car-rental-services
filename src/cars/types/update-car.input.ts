import { Field, InputType} from '@nestjs/graphql';
import { CreateCarInput } from './create-car.input';
import { PartialType } from '@nestjs/mapped-types';


@InputType()
export class UpdateCarInput {

  @Field()
  id: number;

  @Field({nullable:true})
  make: string;

  @Field({nullable:true})
  model: string;

  @Field({nullable:true})
  year: number;

  @Field({nullable:true})
  category: string;

  @Field({nullable:true})
  pricePerDay: number;

}
