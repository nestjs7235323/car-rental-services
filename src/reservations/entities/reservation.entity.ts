import { Entity,Column ,PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { Field,ObjectType } from "@nestjs/graphql";
import { User } from "src/users/user.entity";
import { Car } from "src/cars/entities/car.entity";


@ObjectType()
@Entity()
export class Reservation {

    @Field()
    @PrimaryGeneratedColumn()
    id:number;




    @Field()
    @ManyToOne(()=> User , user => user.reservation)
    user: number;

    @ManyToOne(() => Car, car => car.reservations)
    car: number;


    @Field()
    @Column({ nullable: true })
    startDate: Date;

    @Field()
    @Column({ nullable: true })
    endDate: Date;

    @Field()
    @Column({ nullable: true })
    reservationPrice: number;
    

}
