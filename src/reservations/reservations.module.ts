import { Module } from '@nestjs/common';
import { ReservationsService } from './reservations.service';
import { ReservationsResolver } from './reservations.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reservation } from './entities/reservation.entity';
import { CarsService } from '../cars/cars.service';
import { CarsModule } from 'src/cars/cars.module';
import { Car } from 'src/cars/entities/car.entity';



@Module({
  imports: [
    TypeOrmModule.forFeature([Reservation,Car]),
    CarsModule,
  ],
  providers: [ReservationsResolver, ReservationsService],
  exports: [ReservationsService]

})
export class ReservationsModule {}
