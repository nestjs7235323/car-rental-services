import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { ReservationsService } from './reservations.service';
import { CreateReservationInput } from './types/create-reservation.input';
import { UpdateReservationInput } from './types/update-reservation.input';
import { Reservation } from './entities/reservation.entity';
import { UseGuards } from '@nestjs/common';
import { CurrentUser } from '../decorators/current-user.decorator';
import { User } from '../users/user.entity';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { Car } from 'src/cars/entities/car.entity';
import { checkAvaiibleCarsInput } from './types/check-availibility.input';
import { makeArray } from 'cypress/types/jquery';


@Resolver('Reservation')
export class ReservationsResolver {
  constructor(private readonly reservationsService: ReservationsService) {}

  @Mutation(returns => Reservation || Boolean)
  @UseGuards(JwtAuthGuard)
  createReservation(@Args('createReservationInput') createReservationInput: CreateReservationInput,@CurrentUser() user:User) {
    return this.reservationsService.create(createReservationInput,user);
  }

  @Query(returns => Reservation)
  findAllReservations() {
    return this.reservationsService.findAll();
  }

  @Query(returns => Reservation)
  findOneReservation(@Args('id') id: number) {
    return this.reservationsService.findOne(id);
  }

  @Mutation(returns => Reservation)
  updateReservation(@Args('updateReservationInput') updateReservationInput: UpdateReservationInput) {
    return this.reservationsService.update(updateReservationInput.id, updateReservationInput);
  }

  @Mutation(returns => Reservation)
  removeReservation(@Args('id') id: number) {
    return this.reservationsService.remove(id);
  }



  @Query(returns => [Car])
  findAvailableReservation(
    @Args('checkAvailibeCarsInput') input: checkAvaiibleCarsInput,
  ) {
    return this.reservationsService.checkAvaiibleCars(input);
  }


}
