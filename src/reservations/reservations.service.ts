import { Injectable } from '@nestjs/common';
import { CreateReservationInput } from './types/create-reservation.input';
import { UpdateReservationInput } from './types/update-reservation.input';
import { Reservation } from './entities/reservation.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/user.entity';
import { Car } from 'src/cars/entities/car.entity';
import { CarsService } from 'src/cars/cars.service';

@Injectable()
export class ReservationsService {
  constructor(
    @InjectRepository(Reservation)
    private readonly resRepo: Repository<Reservation>,
    @InjectRepository(Car) private readonly carRepo: Repository<Car>,
    private readonly carService: CarsService,
  ) {}

  async create(input: CreateReservationInput, user: User) {
    const startDate = new Date(input.startDate);
    const endDate = new Date(input.endDate);
  }

  findAll() {
    return `This action returns all reservations`;
  }

  findOne(id: number) {
    return `This action returns a #${id} reservation`;
  }
  d;

  async checkAvaiibleCars(input) {
    const startDate = new Date(input.startDate);
    const endDate = new Date(input.endDate);

    const results = await this.carRepo
      .createQueryBuilder('car')
      .where(
        `car.id NOT IN 
            (SELECT reservation.carId 
             FROM reservation 
             WHERE reservation.startDate BETWEEN :startDate AND :endDate 
             OR reservation.endDate BETWEEN :startDate AND :endDate)`,
      )
      .setParameters({ startDate: startDate, endDate: endDate })
      .getMany();

    console.log(results);

    return results;
  }

  update(id: number, updateReservationInput: UpdateReservationInput) {
    return `This action updates a #${id} reservation`;
  }

  remove(id: number) {
    return `This action removes a #${id} reservation`;
  }
}
