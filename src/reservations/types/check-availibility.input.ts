import { Field, InputType } from '@nestjs/graphql';



@InputType()
export class checkAvaiibleCarsInput {

  @Field()
  startDate: String;

  @Field()
  endDate: String;
}
