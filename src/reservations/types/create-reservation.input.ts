import { Field, InputType } from "@nestjs/graphql";

@InputType()
export class CreateReservationInput {

    @Field()
    carId: number;

    @Field()
    startDate: string;

    @Field()
    endDate: string;

}
