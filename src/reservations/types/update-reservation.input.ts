import { Field, InputType } from '@nestjs/graphql';
import { CreateReservationInput } from './create-reservation.input';
import { PartialType } from '@nestjs/mapped-types';


@InputType()
export class UpdateReservationInput extends PartialType(CreateReservationInput) {

  @Field()
  id: number;
}
