import { Field,Int, ObjectType } from "@nestjs/graphql";
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Reservation } from "src/reservations/entities/reservation.entity";


@Entity()
@ObjectType()
export class User {

    @PrimaryGeneratedColumn()
    @Field(type => Int)
    id: number;

    @Column()
    @Field()
    username: string;

    @Column()
    @Field()
    password: string;

    @OneToMany(() => Reservation, reservation => reservation.user)
    reservation: Reservation[];

}