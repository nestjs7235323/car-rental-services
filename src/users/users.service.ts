import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {
    
    constructor(@InjectRepository(User) private repo: Repository<User> ){}

    async findAllUsers(): Promise<User[]>{

        return this.repo.find()
    }

    
    async createNewUser(data : CreateUserDto): Promise<User> {
        
        const newUser =  this.repo.create(data);

        return await this.repo.save(newUser)
    }


    async findOne(username: string){
        const user = this.repo.findOne({
            where:{
                username
            }
        });

        return user
    }

    
}
